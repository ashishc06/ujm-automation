package base.common;

import com.google.cloud.bigquery.BigQuery;
import common.Configuration;
import common.DBUtil;
import helpers.BigQueryHelper;
import helpers.DataStoreHelper;
import org.testng.annotations.BeforeSuite;

/**
 * @Author AshishC
 */
public class InitializeTestSuite {
    
        /**
         * Method to be invoked before launch of any Suite execution.
         */
        @BeforeSuite(alwaysRun = true)
        public final void init() throws Exception {
            System.out.println("====== SettingUp ViuContentService automation suite execution ======");
            Configuration.getInstance();
            DBUtil dbUtil = new DBUtil();
    
            BigQueryHelper bigQueryHelper = new BigQueryHelper();
            DataStoreHelper dataStoreHelper = new DataStoreHelper();
            
            dbUtil.deleteDataFromSegmentTable("ujm_user_config");
            dbUtil.deleteDataFromSegmentTable("ujm_published_segment");
            
            bigQueryHelper.deleteDataFromTableNew("user_journeys.Automation_Test_Users");
            bigQueryHelper.deleteDataFromTableNew("user_journeys.segments");
            bigQueryHelper.deleteDataFromTableNew("user_journeys.policies");
            bigQueryHelper.deleteDataFromTableNew("user_journeys.journeys");
           
             bigQueryHelper.deleteDataFromTableNew("user_journeys.associations");
            bigQueryHelper.deleteDataFromTableNew("user_journeys.segmented_users");
            bigQueryHelper.deleteDataFromTableNew("user_journeys.policy_journey_rollout");
            bigQueryHelper.deleteDataFromTableNew("user_journeys.users_config_rollout");
            bigQueryHelper.deleteDataFromTableNew("user_journeys.policy_journey_rollout");
 
            
            dataStoreHelper.deleteDataStoreData("Association");
            dataStoreHelper.deleteDataStoreData("BusinessPolicy");
            dataStoreHelper.deleteDataStoreData("Segment");
            dataStoreHelper.deleteDataStoreData("UserJourney");
            
        }
        
        
}
