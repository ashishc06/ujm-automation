package base.common;


import base.testhelpers.ValidationHelper;
import com.fasterxml.jackson.databind.ObjectMapper;
import helpers.RestServiceHelper;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.HashMap;

/**
 * Created by ashish on 12/03/18.
 */
public class TestBase {
    
  
    protected RestServiceHelper restServiceHelper;
    
   
    protected ObjectMapper mapper;
    protected static Logger logger ;
    
    protected ValidationHelper validationHelper;
    
    
    @BeforeClass(alwaysRun = true)
    public void setup() throws IOException {
        
        restServiceHelper = new RestServiceHelper();
        mapper = new ObjectMapper();
        validationHelper= new ValidationHelper();
        
        
    
      
    
       
    }
    
    
}
