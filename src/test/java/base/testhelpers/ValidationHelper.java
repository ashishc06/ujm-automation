package base.testhelpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import listners.ReportListner;
import model.EntityAssociation;
import model.User;
import model.toInsights.*;
import org.testng.Assert;
import org.testng.annotations.Listeners;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Created by ashish on 21/02/18.
 */


@Listeners({ReportListner.class})
public class ValidationHelper {
    
    /**
     *
     * @param segment
     */
    public void validateSegmentCreation(Segment segment){
        
        String pattern = "^(?i)Entity\\sSegment/SG[0-9].*Saved";
        
        Assert.assertTrue(matchResponseFromRegex(segment.getCreationResponse(),pattern));
    
    
    }
    
    /**
     *
     * @param policy
     */
    public void validatePolicyCreation(Policy policy){
        
        String pattern = "^(?i)Entity\\sBusinessPolicy/BP[0-9].*Saved";
        
        Assert.assertTrue(matchResponseFromRegex(policy.getCreationResponse(),pattern));
        
        
    }
    
    
    /**
     *
     * @param entityAssociation
     */
    public void validateEntityAssociationCreation(EntityAssociation entityAssociation){
        
        String pattern = "^(?i)Entity\\sAssociation/AS[0-9].*Saved";
        
        Assert.assertTrue(matchResponseFromRegex(entityAssociation.getCreationResponse(),pattern));
        
        
    }
    
    /**
     *
     * @param s
     * @param pattern
     * @return
     */
    public Boolean matchResponseFromRegex(String s, String pattern){
        
        Pattern p = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
        
        return  p.matcher(s).lookingAt();
        
    }
    
    /**
     *
     * @param expectedPublishNewSegmentResponse
     * @param response
     */
    
    public void validatePublishedSegment(PublishNewSegmentResponse expectedPublishNewSegmentResponse, Response
            response){
    
        FinalResponse finalResponse = response.body().as(FinalResponse.class);
    
        PublishNewSegmentResponse actualPublishNewSegmentResponse = finalResponse.getPublishedSegments()
                .stream().findFirst().get();
        
        for(int i =0; i< expectedPublishNewSegmentResponse.getPolicies().size();i++){
        
            validatePolicyData(expectedPublishNewSegmentResponse.getPolicies().get(i),
                                actualPublishNewSegmentResponse.getPolicies().get(i));
        
        }
       
    }
    
    public void validatePublishedSegment(PublishNewSegmentResponse expectedPublishNewSegmentResponse, PublishNewSegmentResponse
            actualPublishNewSegmentResponse){
        
        
        for(int i =0; i< expectedPublishNewSegmentResponse.getPolicies().size();i++){
            
            validatePolicyData(expectedPublishNewSegmentResponse.getPolicies().get(i),
                    actualPublishNewSegmentResponse.getPolicies().get(i));
            
        }
        
    }
         
         
    public void validatePolicyData(Policy expectedPolicy, Policy actualPolicy){
    
             Assert.assertEquals(actualPolicy.getName(), expectedPolicy.getName(),"Policy Name Mismatch ");
             Assert.assertEquals(actualPolicy.getId(), expectedPolicy.getId(),"Policy ID Mismatch ");
             Assert.assertEquals(actualPolicy.getDescription(), expectedPolicy.getDescription(),"Policy Description " +
                     " Mismatch ");
             Assert.assertEquals(actualPolicy.getCustomizationPointId(), expectedPolicy.getCustomizationPointId(),
                     "Policy Customization Id Mismatch" );
             Assert.assertEquals(actualPolicy.getOwner(), expectedPolicy.getOwner(),"Policy Owner Mismatch ");
             Assert.assertEquals(actualPolicy.getAdditionalProperties().get("priority"), expectedPolicy
                     .getAdditionalProperties().get("priority"),"Policy " + "Priority " + "Mismatch ");
             
        
             validatePolicyActionData(expectedPolicy.getAction(),actualPolicy.getAction());
    
    }
         
         public void validatePolicyActionData(Action expectedAction, Action actualAction){
         
             Assert.assertEquals(actualAction.getName(),expectedAction.getName());
             
             for(int i =0; i< expectedAction.getAttributes().size(); i++){
                 
                 validateAttributesData(expectedAction.getAttributes().get(i),
                                        actualAction.getAttributes().get(i));
             
                 validatePrecondition(expectedAction.getPrecondition(), actualAction.getPrecondition());
             }
         }
         
         public void validateAttributesData(Attribute expectedAttribute, Attribute actualAttribute){
             
             Assert.assertEquals(actualAttribute.getLabel(),expectedAttribute.getLabel());
             Assert.assertEquals(actualAttribute.getValue(),expectedAttribute.getValue());
             Assert.assertEquals(actualAttribute.getType(),expectedAttribute.getType());
         
         }
        
         public void validatePrecondition(Precondition expectedPrecondition, Precondition actualPrecondition){
             
             Assert.assertEquals(actualPrecondition.getLeft(),expectedPrecondition.getLeft());
             Assert.assertEquals(actualPrecondition.getRight(),expectedPrecondition.getRight());
             Assert.assertEquals(actualPrecondition.getOperator(),expectedPrecondition.getOperator());
         }
         
         
         public void validateUserConfigData(Map<String,String> userConfigData, List<User> userList){
    
             ObjectMapper mapper = new ObjectMapper();
             
             
             
         
         
         }

    


}