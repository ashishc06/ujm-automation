package suites.OLD_UJM_Functional;

import base.common.TestBase;
import helpers.RestServiceHelper;
import model.toInsights.Policy;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by ashish on 18/03/18.
 */
public class VerifyPolicies extends TestBase {
    
    
    @Test(dataProviderClass = DataProviders.class,dataProvider = "getCreatePolicy1",groups = "p",enabled = false)
    public void verify_policy_creation(Policy policy,ITestContext iTestContext) throws IOException {
    
        RestServiceHelper restServiceHelper = new RestServiceHelper();
        
        Policy policyCreated = restServiceHelper.createPolicy(policy);
        
        iTestContext.setAttribute("policyId","BP"+policyCreated.getId());
    
        iTestContext.setAttribute("policy_Test1",policy);
        
        validationHelper.validatePolicyCreation(policyCreated);
        
    
    }
    
    @Test(enabled = false)
    public void verify_policy_updation(){
    
    }
    
    @Test(enabled = false)
    public void verify_policy_deletion(){
    
    }
    
    @Test(enabled = false)
    public void verify_duplicate_policy_creation(){
    
    }
    @Test(enabled = false)
    public void verify_multiple_customization_points(){
    
    }
}
