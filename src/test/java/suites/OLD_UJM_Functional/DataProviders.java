package suites.OLD_UJM_Functional;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.toInsights.Policy;
import model.toInsights.Segment;
import org.testng.annotations.DataProvider;
import utils.FileUtil;

import java.io.IOException;

/**
 * Created by ashish on 20/03/18.
 */
public class DataProviders {
    
    @DataProvider(name = "getCreatePolicy1")
    public static Object[][] getCreatePolicy1() throws IOException {
        
        try {
            
            String content = FileUtil.getFileData("CreatePolicy_Test1.json");
            
            ObjectMapper mapper = new ObjectMapper();
            
            Policy policy = mapper.readValue(content, Policy.class);
            
            Object[][] returnValue = new Object[1][1];
            
            for (Object[] each : returnValue) {
                each[0] = policy;
            }
            
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    @DataProvider(name = "getCreateSegment1")
    public static Object[][] getCreateSegment1() throws IOException {
        
        try {
            
            String content = FileUtil.getFileData("SegmentCreation/CreateSegment_Test1.json");
            
            ObjectMapper mapper = new ObjectMapper();
            
            Segment segment = mapper.readValue(content, Segment.class);
            
            Object[][] returnValue = new Object[1][1];
            
            for (Object[] each : returnValue) {
                each[0] = segment;
            }
            
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}