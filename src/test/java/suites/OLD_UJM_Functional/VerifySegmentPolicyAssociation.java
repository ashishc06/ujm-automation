package suites.OLD_UJM_Functional;

import base.common.TestBase;
import model.EntityAssociation;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by ashish on 18/03/18.
 */
public class VerifySegmentPolicyAssociation extends TestBase {

    @Test(dependsOnGroups = {"s","p"},groups = "e",enabled = false)
    public void verify_single_policy_association_to_segment(ITestContext iTestContext) throws IOException {
    
        EntityAssociation entityAssociation = new EntityAssociation();
        entityAssociation.setSegmentId((String)iTestContext.getAttribute("segmentId"));
        entityAssociation.setPolicyId((String)iTestContext.getAttribute("policyId"));
        entityAssociation.setPercentage("100");
        entityAssociation.setCustomizationPoint("cpEnumerationValue");
        
        EntityAssociation entityAssociationCreated = restServiceHelper.createEntityAssociation(entityAssociation);
        
        iTestContext.setAttribute("entityAssociation_Test1",entityAssociation);
        
        validationHelper.validateEntityAssociationCreation(entityAssociationCreated);
    
        
    }
    
    @Test(enabled = false)
    public void verify_multiple_policy_association_to_segment(){
    
    }
    
    @Test(enabled = false)
    public void verify_multiple_policy_association_to_segment_for_same_customization_point(){
    
    }
    
    @Test(enabled = false)
    public void verify_action(){
    
    }
    
    @Test(enabled = false)
    public void verify_association_updation(){
    
    }
    
    @Test(enabled = false)
    public void verify_new_policy_addition_to_association(){
    
    }
    
    @Test(enabled = false)
    public void verify_policy_deletion_from_association(){
    
    }
    
    




}
