package suites.OLD_UJM_Functional;

import base.common.TestBase;
import helpers.RestServiceHelper;
import model.toInsights.Segment;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import java.io.IOException;

/**
 * Created by ashish on 18/03/18.
 */
public class VerifySegments extends TestBase {
    
    
    
    @Test(dataProviderClass = DataProviders.class,dataProvider = "getCreateSegment1",groups = "s",enabled = false)
    public void verify_segment_creation(Segment segment, ITestContext iTestContext) throws IOException {
    
        RestServiceHelper restServiceHelper = new RestServiceHelper();
    
        Segment segmentCreated = restServiceHelper.createSegment(segment);
    
        iTestContext.setAttribute("segmentId","SG"+segmentCreated.getId());
        
        iTestContext.setAttribute("segment_Test1",segment);
        
        validationHelper.validateSegmentCreation(segmentCreated);
      
    }
    
    @Test(enabled = false)
    public void verify_segment_deletion(){
    
    }
    @Test(enabled = false)
    public void verify_segment_updation(){
    
    }
    
    @Test(enabled = false)
    public void verify_duplicate_segments_creation_not_allowed(){
    
    }
    
    
    
}
