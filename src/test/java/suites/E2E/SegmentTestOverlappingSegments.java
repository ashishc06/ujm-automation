package suites.E2E;

import base.testhelpers.ValidationHelper;
import common.DBUtil;
import enums.PolicyTypes;
import enums.SegmentTypes;
import helpers.BigQueryHelper;
import helpers.PolicyHelper;
import helpers.RestServiceHelper;
import helpers.SegmentHelper;
import io.restassured.response.Response;
import model.EntityAssociation;
import model.User;
import model.toInsights.Policy;
import model.toInsights.Segment;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ashish on 02/04/18.
 */
public class SegmentTestOverlappingSegments {
    
    @DataProvider(name = "segmentTestWithOvlpSegments")
    public static Object[][] segmentTestWithOvlpSegments() throws IOException {
        
        try {
            List<User> userList = Stream.of(
                    new User("ovlp_viu-user11","TEST","TEST","TEST","ovlp_test","30"),
                    new User("ovlp_viu-user12","TEST","TEST","TEST","ovlp_test","30"),
                    new User("viu-user13","TEST","TEST","TEST","ovlp_test","30"),
                    new User("ovlp_viu-user14","TEST","TEST","TEST","test","30")
                    
            ).collect(Collectors.toList());
            
            Object[][] returnValue = new Object[1][1];
            
            for (Object[] each : returnValue) {
                each[0] = userList;
            }
            
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    @Test(enabled = false,dataProvider = "segmentTestWithOvlpSegments",groups = "e2e")
    public void verifyOverlappingSegments(List<User> userList) throws IOException, TimeoutException,
            InterruptedException, SQLException {
    
    
        BigQueryHelper bigQueryHelper = new BigQueryHelper();
        
        bigQueryHelper.createUsersInBigQueryDB(userList);
        
        
        SegmentHelper segmentHelper = new SegmentHelper();
        RestServiceHelper restServiceHelper = new RestServiceHelper();
        PolicyHelper policyHelper = new PolicyHelper();
        
        List<Segment> segmentList = new ArrayList();
        Segment segmentHP = segmentHelper.createSegment(SegmentTypes.Segment_Ovlp_high_priority);
        Segment segmentLP = segmentHelper.createSegment(SegmentTypes.Segment_Ovlp_low_priority);
    
        segmentList.add(segmentHP);
        segmentList.add(segmentLP);
        
        List<Policy> policyList = new ArrayList<>();
        Policy policyMidRoll = policyHelper.createPolicy(PolicyTypes.Policy_Midroll);
        Policy policyPreRoll = policyHelper.createPolicy(PolicyTypes.Policy_PreRoll_With_PreCondition);
        
      //  policyList.add(policy);
    
    
        EntityAssociation entityAssociationHP = new EntityAssociation();
        entityAssociationHP.setSegmentId("SG"+segmentHP.getId().toString());
        entityAssociationHP.setPolicyId("BP"+policyMidRoll.getId().toString());
        entityAssociationHP.setPercentage("100");
        entityAssociationHP.setCustomizationPoint(policyMidRoll.getCustomizationPointId());
    
        EntityAssociation entityAssociationCreated = restServiceHelper.createEntityAssociation(entityAssociationHP);
    
        EntityAssociation entityAssociationLP = new EntityAssociation();
        entityAssociationLP.setSegmentId("SG"+segmentLP.getId().toString());
        entityAssociationLP.setPolicyId("BP"+policyPreRoll.getId().toString());
        entityAssociationLP.setPercentage("100");
        entityAssociationLP.setCustomizationPoint(policyPreRoll.getCustomizationPointId());
    
        EntityAssociation entityAssociationCreated1 = restServiceHelper.createEntityAssociation(entityAssociationLP);
    
    
        Response response = restServiceHelper.publishSegment(segmentList);
        
        Thread.sleep(420000);
        DBUtil dbUtil = new DBUtil();
        
        Map<String,String> userConfigData = dbUtil.getUserConfigFromDB(userList);
        
        System.out.println(userConfigData);
        
        
        for(String s :userConfigData.keySet()){
            System.out.println("Key: "+s+", Value : "+ userConfigData.get(s));
        }
        
        ValidationHelper validationHelper = new ValidationHelper();
        System.out.println("Test Finished");
        
    }
 
}
