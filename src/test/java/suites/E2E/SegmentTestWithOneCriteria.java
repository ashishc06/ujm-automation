package suites.E2E;

import base.testhelpers.ValidationHelper;
import common.DBUtil;
import enums.PolicyTypes;
import enums.SegmentTypes;
import helpers.BigQueryHelper;
import helpers.PolicyHelper;
import helpers.RestServiceHelper;
import helpers.SegmentHelper;
import io.restassured.response.Response;
import model.EntityAssociation;
import model.User;
import model.toInsights.Policy;
import model.toInsights.Segment;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ashish on 02/04/18.
 */
public class SegmentTestWithOneCriteria {
    
    @DataProvider(name = "segmentTestWithOneCriteria")
    public static Object[][] segmentTestWithOneCriteria() throws IOException {
        
        try {
            List<User> userList = Stream.of(
                    new User("viu-user1-segment-with-one-criteria","TEST","TEST","TEST","SEG_ONE","20"),
                    new User("viu-user2-segment-with-one-criteria","TEST","TEST","TEST","SEG_ONE","20"),
                    new User("viu-user3-segment-with-one-criteria","TEST","TEST","TEST","SEG_ONE","20"),
                    new User("viu-user4-segment-with-one-criteria","TEST","TEST","TEST","SEG_TEST","20")
                    
            ).collect(Collectors.toList());
            
            Object[][] returnValue = new Object[1][1];
            
            for (Object[] each : returnValue) {
                each[0] = userList;
            }
            
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    @Test(enabled = true,dataProvider = "segmentTestWithOneCriteria",groups = "e2e")
    public void verifySegmentWithOneCriteria(List<User> userList) throws IOException, TimeoutException, InterruptedException, SQLException {
    
    
        BigQueryHelper bigQueryHelper = new BigQueryHelper();
        
        bigQueryHelper.createUsersInBigQueryDB(userList);
        
        
        SegmentHelper segmentHelper = new SegmentHelper();
        RestServiceHelper restServiceHelper = new RestServiceHelper();
        PolicyHelper policyHelper = new PolicyHelper();
        List<Segment> segmentList = new ArrayList();
        
        
        Segment segment = segmentHelper.createSegment(SegmentTypes.Segment_One_Criteria);
        segmentList.add(segment);
        
        List<Policy> policyList = new ArrayList<>();
        Policy policy = policyHelper.createPolicy(PolicyTypes.Policy_Midroll);
        policyList.add(policy);
    
    
        EntityAssociation entityAssociation = new EntityAssociation("SG"+segment.getId().toString(),"BP"+policy.getId()
                .toString(),"25",policy.getCustomizationPointId());
        
        EntityAssociation entityAssociationCreated = restServiceHelper.createEntityAssociation(entityAssociation);
    
    
        Response response = restServiceHelper.publishSegment(segmentList);
        
        Thread.sleep(420000);
        DBUtil dbUtil = new DBUtil();
        
        Map<String,String> userConfigData = dbUtil.getUserConfigFromDB(userList);
        
        for(String s :userConfigData.keySet()){
            
            
            System.out.println("Key: "+s+", Value : "+ userConfigData.get(s));
        }
        
        ValidationHelper validationHelper = new ValidationHelper();
        
        System.out.println("Test Finished");
        
    }
 
}
