package suites.E2E;

import base.testhelpers.ValidationHelper;
import common.DBUtil;
import enums.PolicyTypes;
import enums.SegmentTypes;
import helpers.BigQueryHelper;
import helpers.PolicyHelper;
import helpers.RestServiceHelper;
import helpers.SegmentHelper;
import io.restassured.response.Response;
import model.EntityAssociation;
import model.User;
import model.toInsights.Policy;
import model.toInsights.Segment;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ashish on 02/04/18.
 */
public class DefaultSegmentTest {
    
    @DataProvider(name = "defaultSegmentTest")
    public static Object[][] defaultSegmentTest() throws IOException {
        
        try {
            List<User> userList = Stream.of(
                    new User("viu-user31","TEST","TEST","TEST","SEG_ONE","20"),
                    new User("viu-use32","TEST","TEST","TEST","SEG_ONE","20"),
                    new User("viu-user33","TEST","TEST","TEST","SEG_ONE","20"),
                    new User("viu-user34","TEST","TEST","TEST","SEG_TEST","20")
                    
            ).collect(Collectors.toList());
            
            Object[][] returnValue = new Object[1][1];
            
            for (Object[] each : returnValue) {
                each[0] = userList;
            }
            
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    @Test(enabled = false,dataProvider = "defaultSegmentTest",groups = "e2e")
    public void verifyDefaultSegment(List<User> userList) throws IOException, TimeoutException,
            InterruptedException, SQLException {
    
    
        BigQueryHelper bigQueryHelper = new BigQueryHelper();
        
        bigQueryHelper.createUsersInBigQueryDB(userList);
        
        
        SegmentHelper segmentHelper = new SegmentHelper();
        RestServiceHelper restServiceHelper = new RestServiceHelper();
        PolicyHelper policyHelper = new PolicyHelper();
        List<Segment> segmentList = new ArrayList();
        
        
        Segment segment = segmentHelper.createSegment(SegmentTypes.Segment_Default);
        segmentList.add(segment);
        
        List<Policy> policyList = new ArrayList<>();
        Policy policy = policyHelper.createPolicy(PolicyTypes.Policy_PreRoll_Without_PreCondition);
        policyList.add(policy);
    
    
        EntityAssociation entityAssociation = new EntityAssociation();
        entityAssociation.setSegmentId("SG"+segment.getId().toString());
        entityAssociation.setPolicyId("BP"+policy.getId().toString());
        entityAssociation.setPercentage("100");
        entityAssociation.setCustomizationPoint(policy.getCustomizationPointId());
    
        EntityAssociation entityAssociationCreated = restServiceHelper.createEntityAssociation(entityAssociation);
    
    
        Response response = restServiceHelper.publishSegment(segmentList);
        
        Thread.sleep(420000);
        DBUtil dbUtil = new DBUtil();
        
        Map<String,String> userConfigData = dbUtil.getUserConfigFromDB(userList);
        
        for(String s :userConfigData.keySet()){
            
            System.out.println("Key: "+s+", Value : "+ userConfigData.get(s));
        }
        
        ValidationHelper validationHelper = new ValidationHelper();
        
        System.out.println("Test Finished");
        
    }
 
}
