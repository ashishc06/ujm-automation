package suites.E2E;

import base.testhelpers.ValidationHelper;
import common.DBUtil;
import enums.PolicyTypes;
import enums.SegmentTypes;
import helpers.BigQueryHelper;
import helpers.PolicyHelper;
import helpers.RestServiceHelper;
import helpers.SegmentHelper;
import io.restassured.response.Response;
import model.EntityAssociation;
import model.User;
import model.toInsights.Policy;
import model.toInsights.Segment;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ashish on 05/04/18.
 */
public class SegmentTestForMultiplePoliciesOnSameCP {
    
    @DataProvider(name = "segmentTestWithMultiplePolicies")
    public static Object[][] segmentTestWithMultiplePolicies() throws IOException {
        
        try {
            List<User> userList = Stream.of(
                    new User("viu-user21-segment-with-one-criteria","TEST","IN","TEST","TEST_MP_123","20"),
                    new User("viu-user22-segment-with-one-criteria","TEST","IN","TEST","TEST_MP_TEST","20"),
                    new User("viu-user23-segment-with-one-criteria","TEST","TEST","TEST","TEST_MP_123","20"),
                    new User("viu-user24-segment-with-one-criteria","12344","IN","TEST","TEST_MP123","50")
            
            ).collect(Collectors.toList());
            
            Object[][] returnValue = new Object[1][1];
            
            for (Object[] each : returnValue) {
                each[0] = userList;
            }
            
            return returnValue;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    @Test(enabled = false,dataProvider = "segmentTestWithMultiplePolicies")
    public void verifySegmentWithMultiplePolicies(List<User> userList) throws IOException, TimeoutException,
            InterruptedException, SQLException {
        
        
        BigQueryHelper bigQueryHelper = new BigQueryHelper();
        
        bigQueryHelper.createUsersInBigQueryDB(userList);
        
        
        SegmentHelper segmentHelper = new SegmentHelper();
        RestServiceHelper restServiceHelper = new RestServiceHelper();
        PolicyHelper policyHelper = new PolicyHelper();
        
        List<Segment> segmentList = new ArrayList();
        Segment segment = segmentHelper.createSegment(SegmentTypes.Segment_Multiple_policies);
        segmentList.add(segment);
        
        List<Policy> policyList = new ArrayList<>();
        Policy policyMidroll = policyHelper.createPolicy(PolicyTypes.Policy_Midroll);
        Policy policyPreRoll= policyHelper.createPolicy(PolicyTypes.Policy_PreRoll_Without_PreCondition);
        
        policyList.add(policyMidroll);
        policyList.add(policyPreRoll);
        
        
        
        EntityAssociation entityAssociationP1 = new EntityAssociation();
        entityAssociationP1.setSegmentId("SG"+segment.getId().toString());
        entityAssociationP1.setPolicyId("BP"+policyMidroll.getId().toString());
        entityAssociationP1.setPercentage("100");
        entityAssociationP1.setCustomizationPoint(policyMidroll.getCustomizationPointId());
        
        EntityAssociation entityAssociationCreated1 = restServiceHelper.createEntityAssociation(entityAssociationP1);
    
    
        EntityAssociation entityAssociationP2 = new EntityAssociation();
        entityAssociationP2.setSegmentId("SG"+segment.getId().toString());
        entityAssociationP2.setPolicyId("BP"+policyPreRoll.getId().toString());
        entityAssociationP2.setPercentage("100");
        entityAssociationP2.setCustomizationPoint(policyPreRoll.getCustomizationPointId());
    
        EntityAssociation entityAssociationCreated2 = restServiceHelper.createEntityAssociation(entityAssociationP2);
    
    
    
    
        Response response = restServiceHelper.publishSegment(segmentList);
        
        Thread.sleep(420000);
        DBUtil dbUtil = new DBUtil();
        
        Map<String,String> userConfigData = dbUtil.getUserConfigFromDB(userList);
       
        for(String s :userConfigData.keySet()){
            
            
            System.out.println("Key: "+s+", Value : "+ userConfigData.get(s));
        }
        
        ValidationHelper validationHelper = new ValidationHelper();
        
        System.out.println("Test Finished");
        
    }
}
