package suites.UJM_To_Insights;

import base.common.TestBase;
import common.DBUtil;
import helpers.RestServiceHelper;
import io.restassured.response.Response;
import model.EntityAssociation;
import model.toInsights.Policy;
import model.toInsights.PublishNewSegmentResponse;
import model.toInsights.Segment;
import org.testng.ITestContext;
import org.testng.annotations.Test;
import sun.net.www.content.text.Generic;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashish on 21/03/18.
 */
public class VerifyPublishSegment extends TestBase {
    
    
    
    @Test(dependsOnGroups = {"s","p","e"},enabled = false)
    public void verify_single_policy_push_to_segment(ITestContext iTestContext) throws IOException, SQLException, InterruptedException {
    
        RestServiceHelper restServiceHelper = new RestServiceHelper();
    
        List<Segment> segmentList = new ArrayList<>();
        List<Policy> policyList = new ArrayList<>();
        List<EntityAssociation> entityAssociationList = new ArrayList<>();
        
        PublishNewSegmentResponse publishNewSegmentResponse = new PublishNewSegmentResponse();
        
        segmentList.add((Segment)iTestContext.getAttribute("segment_Test1"));
        policyList.add((Policy) iTestContext.getAttribute("policy_Test1"));
        entityAssociationList.add((EntityAssociation) iTestContext.getAttribute("entityAssociation_Test1"));
        
        publishNewSegmentResponse.setPolicies(policyList);
        publishNewSegmentResponse.setSegment(segmentList.stream().findFirst().get());
        
        Response response =restServiceHelper.publishSegment(segmentList);
        
        validationHelper.validatePublishedSegment(publishNewSegmentResponse,response);
        
        DBUtil dbUtil = new DBUtil();
        
        Thread.sleep(10000);
        
        String sqlResponse = dbUtil.getPublishedSegmentFromDB((Segment)iTestContext.getAttribute("segment_Test1"));
        
        PublishNewSegmentResponse publishNewSegmentResponse1 = mapper.readValue(sqlResponse,PublishNewSegmentResponse
                .class);
    
        validationHelper.validatePublishedSegment(publishNewSegmentResponse,publishNewSegmentResponse1);
        
        
    
    }
}
