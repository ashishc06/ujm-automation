package helpers;

import com.google.api.services.bigquery.model.*;
import com.google.cloud.bigquery.BigQuery;
import com.google.common.base.Joiner;
import model.User;
import org.joda.time.DateTime;
import utils.BigQueryUtil;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeoutException;

/**
 * Created by ashish on 29/03/18.
 */
public class BigQueryHelper {
    
    private BigQuery bigQuery ;
    
    public BigQueryHelper() throws IOException {
        bigQuery=  BigQueryUtil.createAuthorizedClient();
        
        
    }
    
    
    public void executeQuery(String query) throws TimeoutException, InterruptedException {
        
        System.out.println(query);
        BigQueryUtil.runStandardSqlQuery(query,bigQuery);
        
    }
    
    public void deleteDataFromTable(String table) throws TimeoutException, InterruptedException {
    
        String query= "DELETE FROM "+table+" where 1=1;";
        executeQuery(query);
    }
    
    public void deleteDataFromTableNew(String table) throws TimeoutException, InterruptedException {
        
        String query= "SELECT * FROM  "+table+" where false;";
        String dataSetName= table.split("\\.")[0];
        String tableName= table.split("\\.")[1];
        
        System.out.println("Deleting Data From table : " +table);
        
        
        BigQueryUtil.runStandardSqlQueryDelete(query,bigQuery,tableName,dataSetName);
    }
    
    public void createUsersInBigQueryDB(List<User> userList) throws TimeoutException, InterruptedException{
    
        String query= "INSERT INTO user_journeys.Automation_Test_Users (amplitude_id,user_id,country,carrier,campaign,age_on_viu) " +
                "values";
        userList.stream().forEach(user -> {
            try {
                executeQuery(query + "(\"" + user.getAmplitudeId() + "\",\"" + user.getUserId() + "\",\"" +
                        user.getCountry() + "\",\"" + user.getCarrier() + "\",\"" + user.getCampaign() + "\"," +
                        Integer.parseInt(user.getAgeOnViu()) + ");"
                );
            } catch (TimeoutException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
      
    }
    
    public void loadJob(){
    
        // Table reference
        TableReference tableReference = new TableReference()
                .setProjectId("user-journey-management-qa")
                .setDatasetId("user_journeys")
                .setTableId("Automation_Test_Users");
        
        
        // Load job configuration
        JobConfigurationLoad loadConfig = new JobConfigurationLoad()
                .setDestinationTable(tableReference)
              //  .setSchema(tableSchema)
                // Data in Json format (could be CSV)
                .setSourceFormat("NEWLINE_DELIMITED_JSON")
                // Table is created if it does not exists
                .setCreateDisposition("CREATE_IF_NEEDED")
                // Append data (not override data)
                .setWriteDisposition("WRITE_APPEND");
        
        
        Job loadJob = new Job()
                .setJobReference(
                        new JobReference()
                                .setJobId(Joiner.on("-").join("INSERT", "user-journey-management-qa", "user_journeys",
                                        "Automation_Test_Users", DateTime.now().toString("dd-MM-yyyy_HH-mm-ss-SSS")))
                                .setProjectId("user-journey-management-qa"))
                .setConfiguration(new JobConfiguration().setLoad(loadConfig));
         
        
    
    }
    
    
    
}
