package helpers;

import common.Configuration;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.with;

/**
 * Created by ashish on 29/03/18.
 */
public class DataStoreHelper {
    
    private RequestSpecification requestSpecification ;
    
    public void deleteDataStoreData(String kind) throws IOException {
    
        requestSpecification = with().baseUri(Configuration.getInstance().getDeletDataStoreApiUrl());
    
        final Response response = given(requestSpecification).contentType(ContentType.JSON)
                .redirects().follow(true).redirects().max(100)
                .log().all().delete("/"+kind);
        
        System.out.println("Delete DataStore Data Response :" + response.body().asString());
    }
}
