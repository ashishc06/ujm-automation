package helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import enums.SegmentTypes;
import model.toInsights.Segment;
import utils.FileUtil;

import java.io.IOException;

/**
 * Created by ashish on 02/04/18.
 */
public class SegmentHelper {
    
    public Segment createSegment(SegmentTypes segmentTypes) throws IOException {
    
        ObjectMapper mapper = new ObjectMapper();
        RestServiceHelper restServiceHelper = new RestServiceHelper();
        
        String content = FileUtil.getFileData("SegmentCreation/"+segmentTypes.getResourceFileName());
        Segment segment = mapper.readValue(content, Segment.class);
        
        return restServiceHelper.createSegment(segment);
    
    }
    
    public Segment createSegment(String fileName) throws IOException {
    
        RestServiceHelper restServiceHelper = new RestServiceHelper();
        ObjectMapper mapper = new ObjectMapper();
    
        String content = FileUtil.getFileData(fileName);
        Segment segment = mapper.readValue(content, Segment.class);
    
        return restServiceHelper.createSegment(segment);
    }
    
    
}
