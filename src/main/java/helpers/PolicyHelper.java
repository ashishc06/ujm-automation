package helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import enums.PolicyTypes;
import model.toInsights.Policy;
import utils.FileUtil;

import java.io.IOException;

/**
 * Created by ashish on 02/04/18.
 */
public class PolicyHelper {
    
    public Policy createPolicy(PolicyTypes policyTypes) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        RestServiceHelper restServiceHelper = new RestServiceHelper();
    
        String content = FileUtil.getFileData("PolicyCreation/"+policyTypes.getResourceFileName());
        Policy policy = mapper.readValue(content, Policy.class);
    
        return restServiceHelper.createPolicy(policy);
        
    }
}
