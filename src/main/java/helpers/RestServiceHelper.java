package helpers;


import common.Configuration;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import model.EntityAssociation;
import model.toInsights.Policy;
import model.toInsights.Segment;
import utils.RestServiceUtil;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.with;

//import model.EntityAssociation;


/**
 * @Author AshishC
 */
public class RestServiceHelper {
    
    
    private RequestSpecification requestSpecification ;
    
    
  
    public Segment createSegment(Segment segment) throws IOException {
        
        requestSpecification = with().baseUri(Configuration.getInstance().getSegmentServiceUrl());
        
        final Response response = given(requestSpecification).contentType(ContentType.JSON)
                                .redirects().follow(true).redirects().max(100)
                                .body(segment).log().all().post("/segments/");
        
        System.out.println("RESPONSE :" +response.body().asString());
    
        segment.setId(RestServiceUtil.getIdFromString(response.body().asString()));
        segment.setCreationResponse(response.body().asString());
        
        return segment;
    }
    
    public Segment deleteSegment(Segment segment) throws IOException {
        
        requestSpecification = with().baseUri(Configuration.getInstance().getSegmentServiceUrl());
        
        final Response response = given(requestSpecification).contentType(ContentType.JSON)
                .redirects().follow(true).redirects().max(100)
                .body(segment).log().all().delete("/segments/delete");
        
        System.out.println("RESPONSE :" +response.body().asString());
        
        segment.setId(RestServiceUtil.getIdFromString(response.body().asString()));
        segment.setCreationResponse(response.body().asString());
        
        return segment;
    }
    
    
    
    public Policy createPolicy(Policy policy) throws IOException {
    
        requestSpecification = with().baseUri(Configuration.getInstance().getPolicyServiceUrl());
    
        final Response response = given(requestSpecification).contentType(ContentType.JSON)
                .redirects().follow(true).redirects().max(100)
                .body(policy).log().all().post("/policies");
    
        System.out.println("Response  :" +response.body().asString());
        
        policy.setId(RestServiceUtil.getIdFromString(response.body().asString()));
        policy.setCreationResponse(response.body().asString());
        
        return policy;
        
    }
    
    
    public EntityAssociation createEntityAssociation(EntityAssociation entityAssociation) throws IOException {
    
        requestSpecification = with().baseUri(Configuration.getInstance().getSegmentServiceUrl());
    
        final Response response = given(requestSpecification).contentType(ContentType.JSON)
                .redirects().follow(true).redirects().max(100)
                .body(entityAssociation).log().all().post("/segments/"+entityAssociation.getSegmentId()
                +"/policy");
        
        System.out.println("RESPONSE : "+response.body().asString());
    
        entityAssociation.setEntityAssociationId(RestServiceUtil.getIdFromString(response.body().asString()).toString
               ());
        entityAssociation.setCreationResponse(response.body().asString());
    
        return entityAssociation;
    }
    
    
    
    
    public Response publishSegment(List<Segment> segmentList) throws IOException {
    
        requestSpecification = with().baseUri(Configuration.getInstance().getSegmentServiceUrl());
        
        List<String> segmentIdList = segmentList.stream().map(segment -> "SG".concat(segment.getId().toString()))
                .collect
                (Collectors.toList
                ());
    
        final Response response = given(requestSpecification).contentType(ContentType.JSON)
                .redirects().follow(true).redirects().max(100)
                .body(segmentIdList).log().all().post("/segments/publish");
    
        
        System.out.println("RESPONSE : "+response.body().asString());
        
        return response;
    
        
    }
    
    
    
    
    
    
}
