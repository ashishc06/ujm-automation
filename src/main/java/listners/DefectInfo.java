package listners;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Author Ashish C
 */

@Retention(RetentionPolicy.RUNTIME)
public @interface DefectInfo {

    String jirId() default "";

    String stepName() default "";
}