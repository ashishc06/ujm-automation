package data_generator;

import utils.FileUtil;

import java.io.IOException;
import java.util.*;

/**
 * Created by ashish on 30/03/18.
 */
public class Criteria {
    
    public Map<String ,List<String>> storeCriteria() throws IOException {
        
        Map<String ,List<String>> criteria = new HashMap<>();
        
        List<String> operatorForUserId=new ArrayList<>();
        operatorForUserId.add("%");
    
        FileUtil fileUtil = new FileUtil();
    
        String propertyFileName= "CriteriaMapping"+".properties";
        Map<String, String > propertyMap =fileUtil.readFromPropertyFile(propertyFileName);
    
        criteria.put("userId",new ArrayList<String>(Arrays.asList(propertyMap.get("userId").split(","))));
        criteria.put("country",new ArrayList<String>(Arrays.asList(propertyMap.get("country").split(","))));
        criteria.put("campaign",new ArrayList<String>(Arrays.asList(propertyMap.get("campaign").split(","))));
        criteria.put("carrier",new ArrayList<String>(Arrays.asList(propertyMap.get("carrier").split(","))));
        criteria.put("ageOnViu",new ArrayList<String>(Arrays.asList(propertyMap.get("ageOnViu").split(","))));
    
        return criteria;
    }
    
    
    public String getRandomCriteria(Map<String, List<String>> criterias){
    
        Random r = new Random();
        
        return criterias.keySet().stream().skip(r.nextInt((criterias.keySet().size() -1))).findFirst().get();
        
    }
    
    public String getRandomOperator(String criteria, Map<String, List<String>> criterias){
    
        Random r = new Random();
        
       return criterias.get(criteria).stream().skip(r.nextInt(criterias.get(criteria).size()-1)).findFirst().get();
       
    }
}
