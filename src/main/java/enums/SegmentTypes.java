package enums;

/**
 * Created by ashish on 02/04/18.
 */
public enum SegmentTypes {

    
    Segment_Default("CreateSegment_Default.json"),
    Segment_One_Criteria("CreateSegment_one_criteria.json"),
    Segment_Multiple_Criteria("CreateSegment_multiple_criteria.json"),
    Segment_Ovlp_high_priority("Ovlp_segment_1.json"),
    Segment_Ovlp_low_priority("Ovlp_segment_2.json"),
    Segment_Multiple_policies("CreateSegment_multiplePolicies.json");
    
    public String resourceFileName;
    
    SegmentTypes(String resourceFileName){
        
        this.resourceFileName = resourceFileName;
    }
    
    
    public String getResourceFileName() {
        return resourceFileName;
    }
}
