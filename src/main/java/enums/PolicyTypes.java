package enums;

/**
 * Created by ashish on 02/04/18.
 */
public enum PolicyTypes {

    Policy_PreRoll_Without_PreCondition("CreatePolicy_PreRoll_withoutPreCondition.json"),
    Policy_PreRoll_With_PreCondition("CreatePolicy_PreRoll_withPreCondition.json"),
    Policy_Midroll("CreatePolicy_MidRoll_withPreCondition.json"),
    Policy_Offers("CreatePolicy_offers.json");
    
    public String resourceFileName;
    
    PolicyTypes(String resourceFileName){
        this.resourceFileName = resourceFileName;
    }
    
    public String getResourceFileName() {
        return resourceFileName;
    }
}
