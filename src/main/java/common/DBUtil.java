package common;

import model.User;
import model.toInsights.Segment;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ashish on 24/03/18.
 */
public class DBUtil {
    
     PreparedStatement preparedStmt;
     ConnectionPool connectionPool = null;
     DataSource dataSource = null;
    
    /**
     * @throws SQLException
     */
    public String getPublishedSegmentFromDB(Segment segment) throws SQLException {
        
        Connection connObj = null;
        try {
            if(connectionPool == null) {
                connectionPool = new ConnectionPool();
                dataSource = connectionPool.setUpPool();
            }
            connObj = dataSource.getConnection();
            connectionPool.printDBStatus();
            
            String query =
                    
                    "SELECT data from ujm_published_segment where segment_id = '"+ segment.getId().toString()+ "' and" +
                            " " +
                            "segment_name = '\""+segment.getName()+"\"' limit 1; ";
            
            System.out.println("QUERY ###### = " + query);
            
            preparedStmt = connObj.prepareStatement(query);
            ResultSet rs = preparedStmt.executeQuery();
            String result = null;
            
            while (rs.next()){
                result = rs.getString("data");
                
            }
            connObj.close();
            return result;
            
        }catch (Exception e){
            e.printStackTrace();
           
        } finally {
            try {
                // Closing PreparedStatement Object
                if(preparedStmt != null) {
                    preparedStmt.close();
                   
                }
            } catch(Exception sqlException) {
            
            }
            return null;
        }
       
      
    }
    
    public void deletePublishedSegmentsFromDB(){
    
        Connection connObj = null;
        try {
        
            if(connectionPool == null) {
                connectionPool = new ConnectionPool();
                dataSource = connectionPool.setUpPool();
            }
        
            System.out.println("\n=====Making A New Connection Object For Db Transaction=====\n");
            connObj = dataSource.getConnection();
            connectionPool.printDBStatus();
            String query =
                
                    "DELETE from ujm_published_segment ";
        
            System.out.println("QUERY ###### = " + query);
            preparedStmt = connObj.prepareStatement(query);
    
            Boolean result = preparedStmt.execute();
            System.out.println("Delete  Result : "+result);
            
            connObj.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void deleteDataFromSegmentTable(String tableName){
    
        Connection connObj = null;
        try {
        
            if(connectionPool == null) {
                connectionPool = new ConnectionPool();
                dataSource = connectionPool.setUpPool();
            }
        
            System.out.println("\n=====Making A New Connection Object For Db Transaction=====\n");
            connObj = dataSource.getConnection();
            connectionPool.printDBStatus();
            String query =
                
                    "DELETE from "+tableName;
        
            System.out.println("QUERY ###### = " + query);
            preparedStmt = connObj.prepareStatement(query);
        
            Boolean result = preparedStmt.execute();
            System.out.println("Delete  Result : "+result);
        
            connObj.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        
    }
    
    
    public Map<String,String> getUserConfigFromDB(List<User> userList) throws SQLException {
    
    
        Map<String,String> userMessage = new HashMap<>();
        
        Connection connObj = null;
        try {
            if(connectionPool == null) {
                connectionPool = new ConnectionPool();
                dataSource = connectionPool.setUpPool();
            }
            connObj = dataSource.getConnection();
            connectionPool.printDBStatus();
            
            for(User user : userList){
    
                String query =
            
                        "SELECT message from ujm_user_config where userId = \""+user.getUserId()+"\" ;";
    
                System.out.println("QUERY ###### = " + query);
                preparedStmt = connObj.prepareStatement(query);
                ResultSet rs = preparedStmt.executeQuery();
                String result = null;
                
                if(rs.next()){
                   
                        result = rs.getString("message");
                        userMessage.put(user.getUserId(),result);
                    
                }else
                {
                    userMessage.put(user.getUserId(),"NO_DATA_FOUND");
                }
            }
        
            
            connObj.close();
            return userMessage;
        
        }catch (Exception e){
            e.printStackTrace();
     
            return null;
        }
        
        
    }
    
}
    
