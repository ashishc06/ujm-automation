package common;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

import javax.sql.DataSource;

/**
 * Created by ashish on 19/01/18.
 */
public class ConnectionPool {

    static final String driver = "com.mysql.jdbc.Driver";
    private static GenericObjectPool gPool = null;
    
    
    public DataSource setUpPool() throws Exception {
    
        Class.forName(driver);
        
        gPool = new GenericObjectPool();
        gPool.setMaxActive(30);
        
        final String dbUrl = Configuration.getInstance().getUjmLogsDbServer()+":"+Configuration
                .getInstance().getDbPort()+"/"+Configuration.getInstance().getUjmLogsDbName();
    
    
        ConnectionFactory cf = new DriverManagerConnectionFactory(dbUrl,Configuration.getInstance().getUjmLogsDbUser
                (),Configuration.getInstance().getUjmLogsPassword());
    
        PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf,gPool,null,null,false,true);
        
        return new PoolingDataSource(gPool);
        
    }
    
    public GenericObjectPool getConnectionPool(){
        return gPool;
        
    }
    
    public void printDBStatus(){
        System.out.println("Max.: " + getConnectionPool().getMaxActive() + "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
    }
    
    
    
    
}
