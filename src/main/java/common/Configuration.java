package common;

import utils.FileUtil;

import java.io.IOException;
import java.util.Map;

/**
 * Created by ashish on 21/02/18.
 */
public class Configuration {
    
    private static Configuration configuration;
    
    private static String segmentServiceUrl;
    
    private static String policyServiceUrl;
    
    private static String deletDataStoreApiUrl;
    
    private static String ujmLogsDbName;
    
    private static String ujmLogsDbUser;
    
    private static String ujmLogsPassword;
    
    private static String ujmLogsDbServer;
    
    private static String dbPort = "3306";
    
    private Configuration() {
    
    }
    
    /**
     *
     * @return
     * @throws IOException
     */
    public static Configuration getInstance() throws IOException {
        
        if(configuration == null) {
            configuration = new Configuration();
            FileUtil fileUtil = new FileUtil();
            
            String envName = System.getProperty("envName").toString();
            String propertyFileName= envName+".properties";
            Map<String, String > propertyMap =fileUtil.readFromPropertyFile(propertyFileName);
            
            segmentServiceUrl = propertyMap.get("segmentServiceUrl");
            policyServiceUrl= propertyMap.get("policySerivceUrl");
            ujmLogsDbName= propertyMap.get("ujmLogsDbName");
            ujmLogsDbUser= propertyMap.get("ujmLogsDbUser");
            ujmLogsPassword= propertyMap.get("ujmLogsPassword");
            ujmLogsDbServer= propertyMap.get("ujmLogsDbServer");
            deletDataStoreApiUrl = propertyMap.get("dataStoreAPIUrl");
            
        }
        
        return configuration;
    }
    
    public String getSegmentServiceUrl() {
        return segmentServiceUrl;
    }
    
    public String getPolicyServiceUrl() {
        return policyServiceUrl;
    }
    
    public  String getUjmLogsDbName() {
        return ujmLogsDbName;
    }
    
    public  String getUjmLogsDbUser() {
        return ujmLogsDbUser;
    }
    
    public  String getUjmLogsPassword() {
        return ujmLogsPassword;
    }
    
    public  String getUjmLogsDbServer() {
        return ujmLogsDbServer;
    }
    
    public  String getDbPort() {
        return dbPort;
    }
    
    public String getDeletDataStoreApiUrl() {
        return deletDataStoreApiUrl;
    }
}
