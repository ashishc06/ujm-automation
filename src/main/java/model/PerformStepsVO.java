package model;

import model.toInsights.Policy;
import model.toInsights.Segment;

import java.util.List;

/**
 * Created by ashish on 02/04/18.
 */
public class PerformStepsVO {
    
    private Segment segment;
    
    private List<Segment> segmentList;
    
    private Policy policy;
    
    private List<Policy> policyList;
    
    
    public Segment getSegment() {
        return segment;
    }
    
    public void setSegment(Segment segment) {
        this.segment = segment;
    }
    
    public List<Segment> getSegmentList() {
        return segmentList;
    }
    
    public void setSegmentList(List<Segment> segmentList) {
        this.segmentList = segmentList;
    }
    
    public Policy getPolicy() {
        return policy;
    }
    
    public void setPolicy(Policy policy) {
        this.policy = policy;
    }
    
    public List<Policy> getPolicyList() {
        return policyList;
    }
    
    public void setPolicyList(List<Policy> policyList) {
        this.policyList = policyList;
    }
}
