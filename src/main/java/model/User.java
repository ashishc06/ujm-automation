package model;

import lombok.Data;



/**
 * Created by ashish on 02/04/18.
 */
@Data
public class User {
    
    private String userId;
    
    private String amplitudeId;
    
    private String country;
    
    private String carrier;
    
    private String campaign;
    
    private String ageOnViu;
    
    public User(String userId, String amplitudeId, String country,String carrier,String campaign, String ageOnViu){
        this.userId = userId;
        this.amplitudeId = amplitudeId;
        this.country = country;
        this.carrier = carrier;
        this.campaign = campaign;
        this.ageOnViu = ageOnViu;
    }
    
}
