
package model.toInsights;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segmentName",
    "segmentVersion",
    "policyJourneyName",
    "policyJourneyVersion",
    "percentageApplication"
})
@JsonIgnoreProperties
public class Association {

    @JsonProperty("segmentName")
    private String segmentName;
    @JsonProperty("segmentVersion")
    private Integer segmentVersion;
    @JsonProperty("policyJourneyName")
    private String policyJourneyName;
    @JsonProperty("policyJourneyVersion")
    private Integer policyJourneyVersion;
    @JsonProperty("percentageApplication")
    private Integer percentageApplication;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("segmentName")
    public String getSegmentName() {
        return segmentName;
    }

    @JsonProperty("segmentName")
    public void setSegmentName(String segmentName) {
        this.segmentName = segmentName;
    }

    @JsonProperty("segmentVersion")
    public Integer getSegmentVersion() {
        return segmentVersion;
    }

    @JsonProperty("segmentVersion")
    public void setSegmentVersion(Integer segmentVersion) {
        this.segmentVersion = segmentVersion;
    }

    @JsonProperty("policyJourneyName")
    public String getPolicyJourneyName() {
        return policyJourneyName;
    }

    @JsonProperty("policyJourneyName")
    public void setPolicyJourneyName(String policyJourneyName) {
        this.policyJourneyName = policyJourneyName;
    }

    @JsonProperty("policyJourneyVersion")
    public Integer getPolicyJourneyVersion() {
        return policyJourneyVersion;
    }

    @JsonProperty("policyJourneyVersion")
    public void setPolicyJourneyVersion(Integer policyJourneyVersion) {
        this.policyJourneyVersion = policyJourneyVersion;
    }

    @JsonProperty("percentageApplication")
    public Integer getPercentageApplication() {
        return percentageApplication;
    }

    @JsonProperty("percentageApplication")
    public void setPercentageApplication(Integer percentageApplication) {
        this.percentageApplication = percentageApplication;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    
    @Override
    public String toString() {
        return "Association{" +
                "segmentName='" + segmentName + '\'' +
                ", segmentVersion=" + segmentVersion +
                ", policyJourneyName='" + policyJourneyName + '\'' +
                ", policyJourneyVersion=" + policyJourneyVersion +
                ", percentageApplication=" + percentageApplication +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
