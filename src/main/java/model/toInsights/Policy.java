
package model.toInsights;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "description",
    "owner",
    "version",
    "customizationPointId",
    "action"
})
@JsonIgnoreProperties
public class Policy {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("owner")
    private String owner;
    @JsonIgnore
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("customizationPointId")
    private String customizationPointId;
    @JsonProperty("action")
    private Action action;
    @JsonIgnore
    private String creationResponse;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("owner")
    public String getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(String owner) {
        this.owner = owner;
    }
    
    @JsonIgnore
    @JsonProperty("version")
    public Integer getVersion() {
        return version;
    }
    @JsonIgnore
    @JsonProperty("version")
    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("customizationPointId")
    public String getCustomizationPointId() {
        return customizationPointId;
    }

    @JsonProperty("customizationPointId")
    public void setCustomizationPointId(String customizationPointId) {
        this.customizationPointId = customizationPointId;
    }

    @JsonProperty("action")
    public Action getAction() {
        return action;
    }

    @JsonProperty("action")
    public void setAction(Action action) {
        this.action = action;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @JsonIgnore
    public String getCreationResponse() {
        return creationResponse;
    }
    @JsonIgnore
    public void setCreationResponse(String creationResponse) {
        this.creationResponse = creationResponse;
    }
    
    @Override
    public String toString() {
        return "Policy{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", owner='" + owner + '\'' +
                ", version=" + version +
                ", customizationPointId='" + customizationPointId + '\'' +
                ", action=" + action +
                ", creationResponse='" + creationResponse + '\'' +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
