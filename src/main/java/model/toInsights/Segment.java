
package model.toInsights;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "description",
    "owner",
    "version",
    "priority",
    "isDefault",
    "criteria"
})
@JsonIgnoreProperties
public class Segment {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("description")
    private String description;
    @JsonProperty("owner")
    private String owner;
    @JsonProperty("version")
    private Integer version;
    @JsonProperty("priority")
    private Integer priority;
    @JsonProperty("isDefault")
    private Boolean isDefault;
    @JsonProperty("criteria")
    private Criteria criteria;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
    @JsonIgnore
    private String creationResponse;

    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("owner")
    public String getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(String owner) {
        this.owner = owner;
    }

    @JsonProperty("version")
    public Integer getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(Integer version) {
        this.version = version;
    }

    @JsonProperty("priority")
    public Integer getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    @JsonProperty("isDefault")
    public Boolean getIsDefault() {
        return isDefault;
    }

    @JsonProperty("isDefault")
    public void setIsDefault(Boolean isDefault) {
        this.isDefault = isDefault;
    }

    @JsonIgnore
    @JsonProperty("criteria")
    public Criteria getCriteria() {
        return criteria;
    }

    @JsonProperty("criteria")
    public void setCriteria(Criteria criteria) {
        this.criteria = criteria;
    }
    
    @JsonIgnore
    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @JsonIgnore
    public String getCreationResponse() {
        return creationResponse;
    }
    @JsonIgnore
    public void setCreationResponse(String creationResponse) {
        this.creationResponse = creationResponse;
    }
    
    @Override
    public String toString() {
        return "Segment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", owner='" + owner + '\'' +
                ", version=" + version +
                ", priority=" + priority +
                ", isDefault=" + isDefault +
                ", criteria=" + criteria +
                ", additionalProperties=" + additionalProperties +
                ", creationResponse='" + creationResponse + '\'' +
                '}';
    }
}
