
package model.toInsights;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "left",
    "operator",
    "right"
})
@JsonIgnoreProperties
public class Left_ {

    @JsonProperty("left")
    private String left;
    @JsonProperty("operator")
    private String operator;
    @JsonProperty("right")
    private Integer right;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("left")
    public String getLeft() {
        return left;
    }

    @JsonProperty("left")
    public void setLeft(String left) {
        this.left = left;
    }

    @JsonProperty("operator")
    public String getOperator() {
        return operator;
    }

    @JsonProperty("operator")
    public void setOperator(String operator) {
        this.operator = operator;
    }

    @JsonProperty("right")
    public Integer getRight() {
        return right;
    }

    @JsonProperty("right")
    public void setRight(Integer right) {
        this.right = right;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @Override
    public String toString() {
        return "Left_{" +
                "left='" + left + '\'' +
                ", operator='" + operator + '\'' +
                ", right=" + right +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
