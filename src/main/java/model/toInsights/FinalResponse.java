package model.toInsights;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by ashish on 21/03/18.
 */
public class FinalResponse {
    
    private List<PublishNewSegmentResponse> publishedSegments;
    
    public List<PublishNewSegmentResponse> getPublishedSegments() {
        return publishedSegments;
    }
    
    public void setPublishedSegments(List<PublishNewSegmentResponse> publishedSegments) {
        this.publishedSegments = publishedSegments;
    }
    
    @Override
    public String toString() {
        return "FinalResponse{" +
                "publishedSegments=" + publishedSegments +
                '}';
    }
}
