
package model.toInsights;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "customizationPointId",
    "action" +
            ""
})
@JsonIgnoreProperties
public class Customization {

    @JsonProperty("customizationPointId")
    private String customizationPointId;
    @JsonProperty("action")
    private Action_ action;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("customizationPointId")
    public String getCustomizationPointId() {
        return customizationPointId;
    }

    @JsonProperty("customizationPointId")
    public void setCustomizationPointId(String customizationPointId) {
        this.customizationPointId = customizationPointId;
    }

    @JsonProperty("action")
    public Action_ getAction() {
        return action;
    }

    @JsonProperty("action")
    public void setAction(Action_ action) {
        this.action = action;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @Override
    public String toString() {
        return "Customization{" +
                "customizationPointId='" + customizationPointId + '\'' +
                ", action=" + action +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
