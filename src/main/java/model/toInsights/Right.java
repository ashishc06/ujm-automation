
package model.toInsights;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "left",
    "operator",
    "right"
})
@JsonIgnoreProperties
public class Right {

    @JsonProperty("left")
    private Left_ left;
    @JsonProperty("operator")
    private String operator;
    @JsonProperty("right")
    private Right_ right;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("left")
    public Left_ getLeft() {
        return left;
    }

    @JsonProperty("left")
    public void setLeft(Left_ left) {
        this.left = left;
    }

    @JsonProperty("operator")
    public String getOperator() {
        return operator;
    }

    @JsonProperty("operator")
    public void setOperator(String operator) {
        this.operator = operator;
    }

    @JsonProperty("right")
    public Right_ getRight() {
        return right;
    }

    @JsonProperty("right")
    public void setRight(Right_ right) {
        this.right = right;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    
    @Override
    public String toString() {
        return "Right{" +
                "left=" + left +
                ", operator='" + operator + '\'' +
                ", right=" + right +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
