
package model.toInsights;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segment",
    "policies",
    "userJourney",
    "associations"
})
@JsonIgnoreProperties
public class PublishNewSegmentResponse {

    @JsonProperty("segment")
    private Segment segment;
    @JsonProperty("policies")
    private List<Policy> policies = null;
    @JsonProperty("userJourney")
    private UserJourney userJourney;
    @JsonProperty("associations")
    private List<Association> associations = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("segment")
    public Segment getSegment() {
        return segment;
    }

    @JsonProperty("segment")
    public void setSegment(Segment segment) {
        this.segment = segment;
    }

    @JsonProperty("policies")
    public List<Policy> getPolicies() {
        return policies;
    }

    @JsonProperty("policies")
    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

    @JsonProperty("userJourney")
    public UserJourney getUserJourney() {
        return userJourney;
    }

    @JsonProperty("userJourney")
    public void setUserJourney(UserJourney userJourney) {
        this.userJourney = userJourney;
    }

    @JsonProperty("associations")
    public List<Association> getAssociations() {
        return associations;
    }

    @JsonProperty("associations")
    public void setAssociations(List<Association> associations) {
        this.associations = associations;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    
    @Override
    public String toString() {
        return "PublishNewSegmentResponse{" +
                "segment=" + segment +
                ", policies=" + policies +
                ", userJourney=" + userJourney +
                ", associations=" + associations +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
