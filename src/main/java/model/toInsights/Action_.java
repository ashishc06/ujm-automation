
package model.toInsights;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "id",
    "version",
    "attributes",
    "precondition"
})
@JsonIgnoreProperties
public class Action_ {

    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private String id;
    @JsonProperty("version")
    private String version;
    @JsonProperty("attributes")
    private List<Attribute_> attributes = null;
    @JsonProperty("precondition")
    private Precondition_ precondition;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("version")
    public String getVersion() {
        return version;
    }

    @JsonProperty("version")
    public void setVersion(String version) {
        this.version = version;
    }

    @JsonProperty("attributes")
    public List<Attribute_> getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(List<Attribute_> attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("precondition")
    public Precondition_ getPrecondition() {
        return precondition;
    }

    @JsonProperty("precondition")
    public void setPrecondition(Precondition_ precondition) {
        this.precondition = precondition;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @Override
    public String toString() {
        return "Action_{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                ", version='" + version + '\'' +
                ", attributes=" + attributes +
                ", precondition=" + precondition +
                ", additionalProperties=" + additionalProperties +
                '}';
    }
}
