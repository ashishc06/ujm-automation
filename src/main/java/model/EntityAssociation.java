package model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ashish on 21/03/18.
 */

public class EntityAssociation {

    @JsonIgnore
    private String segmentId;
    
    @JsonProperty("id")
    private String policyId;
    
    private String percentage;
    
    private String customizationPoint;
    
    @JsonIgnore
    private String entityAssociationId;
    
    @JsonIgnore
    private String creationResponse;
    
    public EntityAssociation(){
    
    }
    
    
    public EntityAssociation(String segmentId, String policyId, String percentage, String customizationPoint){
        this.segmentId = segmentId;
        this.policyId= policyId;
        this.percentage = percentage;
        this.customizationPoint = customizationPoint;
    }
    
    
    
    
    @JsonIgnore
    public String getSegmentId() {
        return segmentId;
    }
    
    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }
    @JsonProperty("id")
    public String getPolicyId() {
        return policyId;
    }
    
    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }
    
    public String getPercentage() {
        return percentage;
    }
    
    public void setPercentage(String percentage) {
        this.percentage = percentage;
    }
    
    public String getCustomizationPoint() {
        return customizationPoint;
    }
    
    public void setCustomizationPoint(String customizationPoint) {
        this.customizationPoint = customizationPoint;
    }
    
    @JsonIgnore
    public String getEntityAssociationId() {
        return entityAssociationId;
    }
    
    
    public void setEntityAssociationId(String entityAssociationId) {
        this.entityAssociationId = entityAssociationId;
    }
    @JsonIgnore
    public String getCreationResponse() {
        return creationResponse;
    }
    @JsonIgnore
    public void setCreationResponse(String creationResponse) {
        this.creationResponse = creationResponse;
    }
    
    @Override
    public String toString() {
        return "EntityAssociation{" +
                "segmentId='" + segmentId + '\'' +
                ", policyId='" + policyId + '\'' +
                ", percentage='" + percentage + '\'' +
                ", customizationPoint='" + customizationPoint + '\'' +
                ", entityAssociationId='" + entityAssociationId + '\'' +
                ", creationResponse='" + creationResponse + '\'' +
                '}';
    }
}
