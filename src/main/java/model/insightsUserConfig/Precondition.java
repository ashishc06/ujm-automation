
package model.insightsUserConfig;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "left",
    "right",
    "operator"
})
public class Precondition {

    @JsonProperty("left")
    private String left;
    @JsonProperty("right")
    private String right;
    @JsonProperty("operator")
    private String operator;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("left")
    public String getLeft() {
        return left;
    }

    @JsonProperty("left")
    public void setLeft(String left) {
        this.left = left;
    }

    @JsonProperty("right")
    public String getRight() {
        return right;
    }

    @JsonProperty("right")
    public void setRight(String right) {
        this.right = right;
    }

    @JsonProperty("operator")
    public String getOperator() {
        return operator;
    }

    @JsonProperty("operator")
    public void setOperator(String operator) {
        this.operator = operator;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
