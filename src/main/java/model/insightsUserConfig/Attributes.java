
package model.insightsUserConfig;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "adSequence",
    "adShow"
})
public class Attributes {

    @JsonProperty("adSequence")
    private String adSequence;
    @JsonProperty("adShow")
    private Boolean adShow;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("adSequence")
    public String getAdSequence() {
        return adSequence;
    }

    @JsonProperty("adSequence")
    public void setAdSequence(String adSequence) {
        this.adSequence = adSequence;
    }

    @JsonProperty("adShow")
    public Boolean getAdShow() {
        return adShow;
    }

    @JsonProperty("adShow")
    public void setAdShow(Boolean adShow) {
        this.adShow = adShow;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
