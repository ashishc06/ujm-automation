
package model.insightsUserConfig;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "segmentVersion",
    "policyVersion",
    "policyId",
    "segmentId",
    "journeyId",
    "journeyVersion"
})
public class Context {

    @JsonProperty("segmentVersion")
    private String segmentVersion;
    @JsonProperty("policyVersion")
    private String policyVersion;
    @JsonProperty("policyId")
    private String policyId;
    @JsonProperty("segmentId")
    private String segmentId;
    @JsonProperty("journeyId")
    private String journeyId;
    @JsonProperty("journeyVersion")
    private String journeyVersion;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("segmentVersion")
    public String getSegmentVersion() {
        return segmentVersion;
    }

    @JsonProperty("segmentVersion")
    public void setSegmentVersion(String segmentVersion) {
        this.segmentVersion = segmentVersion;
    }

    @JsonProperty("policyVersion")
    public String getPolicyVersion() {
        return policyVersion;
    }

    @JsonProperty("policyVersion")
    public void setPolicyVersion(String policyVersion) {
        this.policyVersion = policyVersion;
    }

    @JsonProperty("policyId")
    public String getPolicyId() {
        return policyId;
    }

    @JsonProperty("policyId")
    public void setPolicyId(String policyId) {
        this.policyId = policyId;
    }

    @JsonProperty("segmentId")
    public String getSegmentId() {
        return segmentId;
    }

    @JsonProperty("segmentId")
    public void setSegmentId(String segmentId) {
        this.segmentId = segmentId;
    }

    @JsonProperty("journeyId")
    public String getJourneyId() {
        return journeyId;
    }

    @JsonProperty("journeyId")
    public void setJourneyId(String journeyId) {
        this.journeyId = journeyId;
    }

    @JsonProperty("journeyVersion")
    public String getJourneyVersion() {
        return journeyVersion;
    }

    @JsonProperty("journeyVersion")
    public void setJourneyVersion(String journeyVersion) {
        this.journeyVersion = journeyVersion;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
