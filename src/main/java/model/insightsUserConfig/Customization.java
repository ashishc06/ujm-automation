
package model.insightsUserConfig;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "rollout_flag",
    "context",
    "action",
    "configType",
    "segmentPriority"
})
public class Customization {

    @JsonProperty("rollout_flag")
    private Boolean rolloutFlag;
    @JsonProperty("context")
    private Context context;
    @JsonProperty("action")
    private Action action;
    @JsonProperty("configType")
    private String configType;
    @JsonProperty("segmentPriority")
    private Integer segmentPriority;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("rollout_flag")
    public Boolean getRolloutFlag() {
        return rolloutFlag;
    }

    @JsonProperty("rollout_flag")
    public void setRolloutFlag(Boolean rolloutFlag) {
        this.rolloutFlag = rolloutFlag;
    }

    @JsonProperty("context")
    public Context getContext() {
        return context;
    }

    @JsonProperty("context")
    public void setContext(Context context) {
        this.context = context;
    }

    @JsonProperty("action")
    public Action getAction() {
        return action;
    }

    @JsonProperty("action")
    public void setAction(Action action) {
        this.action = action;
    }

    @JsonProperty("configType")
    public String getConfigType() {
        return configType;
    }

    @JsonProperty("configType")
    public void setConfigType(String configType) {
        this.configType = configType;
    }

    @JsonProperty("segmentPriority")
    public Integer getSegmentPriority() {
        return segmentPriority;
    }

    @JsonProperty("segmentPriority")
    public void setSegmentPriority(Integer segmentPriority) {
        this.segmentPriority = segmentPriority;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
