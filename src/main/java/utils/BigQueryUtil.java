package utils;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.bigquery.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/**
 * Created by ashish on 29/03/18.
 */
public class BigQueryUtil {
    
    public static BigQuery createAuthorizedClient() throws IOException {
        
        GoogleCredentials credentials;
        File credentialsPath = new File("/Users/ashish/Documents/GoogleCredentials/UJMCred.json");  // TODO: update to your key path.
        try (FileInputStream serviceAccountStream = new FileInputStream(credentialsPath)) {
            credentials = ServiceAccountCredentials.fromStream(serviceAccountStream);
        }
        
        // Instantiate a client.
        BigQuery bigquery =
                BigQueryOptions.newBuilder().setCredentials(credentials).build().getService();
        
        
        return bigquery;
    }
    
    public static void runStandardSqlQuery(String queryString, BigQuery bigQuery)
            throws TimeoutException, InterruptedException {
        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(queryString)
                        // To use standard SQL syntax, set useLegacySql to false. See:
                        // https://cloud.google.com/bigquery/docs/reference/standard-sql/enabling-standard-sql
                        .setUseLegacySql(false)
                        .build();
        
        runQuery(queryConfig, bigQuery);
    }
    
    public static void runStandardSqlQueryDelete(String queryString, BigQuery bigQuery,String tableName, String
            dataSetName)
            throws TimeoutException, InterruptedException {
        QueryJobConfiguration queryConfig =
                QueryJobConfiguration.newBuilder(queryString)
                        // To use standard SQL syntax, set useLegacySql to false. See:
                        // https://cloud.google.com/bigquery/docs/reference/standard-sql/enabling-standard-sql
                        .setUseLegacySql(false).setDestinationTable(TableId.of(dataSetName, tableName))
                        .setWriteDisposition(JobInfo.WriteDisposition.WRITE_TRUNCATE)
                        .build();
        
        runQuery(queryConfig, bigQuery);
    }
    
    // [START run_query]
    public static void runQuery(QueryJobConfiguration queryConfig, BigQuery bigquery)
            throws TimeoutException, InterruptedException {
        
        // Create a job ID so that we can safely retry.
        JobId jobId = JobId.of(UUID.randomUUID().toString());
        Job queryJob = bigquery.create(JobInfo.newBuilder(queryConfig).setJobId(jobId).build());
        
        // Wait for the query to complete.
        queryJob = queryJob.waitFor();
        
        // Check for errors
        if (queryJob == null) {
            throw new RuntimeException("Job no longer exists");
        } else if (queryJob.getStatus().getError() != null) {
            // You can also look at queryJob.getStatus().getExecutionErrors() for all
            // errors, not just the latest one.
            throw new RuntimeException(queryJob.getStatus().getError().toString());
        }
        
        QueryResponse response = bigquery.getQueryResults(jobId);
        
        
        // Get the results.
        QueryResult result = queryJob.getQueryResults().getResult();
        
        // Print all pages of the results.
        while (result != null) {
            for (List<FieldValue> row : result.iterateAll()) {
                for (FieldValue val : row) {
                    System.out.printf("%s,", val.toString());
                }
                System.out.printf("\n");
            }
            
            result = result.getNextPage();
        }
    }
    // [END run_query]


}
