package utils;


import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Created by ashish on 16/03/18.
 */
public class FileUtil {
    
    
    /**
     *
      * @param fileName
     * @return
     * @throws IOException
     */
    public static Map<String,String> readFromPropertyFile(String fileName)throws IOException {
        Map<String,String> propertiesMap ;
        FileInputStream fis = null;
        try {
            Properties prop = new Properties();
            fis = new FileInputStream( fileName );
            prop.load(fis);
        
            propertiesMap = new HashMap(prop);
            
            return propertiesMap;
            
        } catch (IOException e) {
            throw new IOException();
        } finally {
            if(fis != null) {
                fis.close();
            }
        }
        
    }
    
    /**
     *
     * @param fileName
     * @return
     */
    public static String getFileData(final String fileName) {
        
        try {
            String content = IOUtils.toString(new FileInputStream(new File("src/test/resources/"+fileName)),"UTF-8");
            
            return content;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static String readJsonFromFile(String fileName) throws IOException {
    
        try{
          
          return new String(Files.readAllBytes(Paths.get("/src/test/resources/"+fileName)));
          
        }catch (Exception e){
            new Throwable(e);
            return null;
        }
        
    }
}
