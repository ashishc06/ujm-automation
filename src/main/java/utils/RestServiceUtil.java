package utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by ashish on 21/03/18.
 */
public class RestServiceUtil {
    
    private static String getStringToken(String s,String tokenizer, String findKey){
        
        return Stream.of(s.split(tokenizer)).collect(Collectors.toList()).stream()
                .filter(f-> f.contains(findKey)).findFirst().get();
        
    }
    
    
    public static Map<String, String> parseParamsFromUrl(String url) {
        
        String urlSplit[] = url.split("\\?");
        String paramSplit[] = urlSplit[1].split("&");
        Map<String, String> paramMap = new HashMap<String, String>();
        
        for (String s : paramSplit) {
            
            paramMap.put(s.split("=")[0], s.split("=")[1]);
            
        }
        return paramMap;
    }
    
    public static Integer getIdFromString(String s){
        
        String pattern = "([0-9]+)";
        Pattern p = Pattern.compile(pattern);
        
        Matcher matcher = p.matcher(s);
        Integer id = null;
        
        while (matcher.find()){
            id= Integer.parseInt(matcher.group());
        }
        return id;
    }
}
